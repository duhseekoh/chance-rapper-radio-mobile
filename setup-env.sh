#!/usr/bin/env bash

ENVJS_FILE="./env-config.js"

# Echo what's happening to the build logs
echo Creating environment config file

# Create `env.js` file in project root
touch $ENVJS_FILE

# Write environment config to file, hiding from build logs
tee $ENVJS_FILE <<EOF
export const API_HOST = '$RAPPER_RADIO_API_URL';
EOF
