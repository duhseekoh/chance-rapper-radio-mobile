/* eslint-disable no-underscore-dangle */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  FlatList,
  Linking,
  WebView,
} from 'react-native';
import { connect } from 'react-redux';
import navOptions from '../constants/navOptions';
import { twitterAppURL, twitterBrowserURL, facebookAppURL, facebookBrowserURL } from '../constants/socialMediaURLs';
import * as fromSongs from '../selectors/songs';
import { changeText, setKeyboardMargin, toggleSuggestions, toggleStations, toggleInput, selectCity, selectStation, selectSong, setTwitterURI, setFacebookURI, resetView, resetCity, resetStation } from '../actions/view';
import * as fromStations from '../selectors/stations';
import { fetchStations, fetchStationsByLatLon } from '../actions/stations';
import Loader from '../components/Loader/Loader';

const scrollView = 'scrollView';

class SongScreen extends Component {
  componentWillMount() {
    this.props.fetchStations();
    this.props.resetView();
  }
  _selectCity(city, lat, lon) {
    this.props.selectCity(city);
    this._onChangeText(city);
    this.props.toggleSuggestions(false);
    this.props.toggleStations(true);
    this.props.toggleInput(false);
    this.props.fetchStationsByLatLon(lat, lon);
  }

  _selectStation(station, cityName, twitter) {
    this.props.selectStation(station);
    this.props.selectCity(cityName);
    this._onChangeText('');

    this.props.setKeyboardMargin(10);

    this.props.toggleSuggestions(false);
    this.props.toggleStations(false);

    this.props.setTwitterURI(encodeURIComponent(this.props.navigation.state.params.song.tweetText.replace('$TWITTER$', twitter)));

    // TODO should set to facebook specific post
    this.props.setFacebookURI(encodeURIComponent(this.props.navigation.state.params.song.tweetText.replace('$TWITTER$', twitter)));
  }

  _onChangeCity() {
    this.props.toggleInput(true);
    this.props.changeText('');
    this.props.resetCity();
    this.props.resetStation();
    this.props.toggleStations(false);
  }

  _onChangeStation() {
    this.props.resetStation();
  }

  _onChangeText(text) {
    this.props.changeText(text);
    this.props.toggleSuggestions(true);
    this.props.toggleStations(false);
  }

  _onFocus = () => {
    this.props.setKeyboardMargin(100);
    // TODO: look into react/no-string-refs error
    this.refs.scrollView.scrollTo({ y: 160 }); // eslint-disable-line
  }

  _onBlur = () => {
    this.props.setKeyboardMargin(10);
  }

  _keyExtractor = (item, index) => index;

  _handleTweet = () => {
    Linking.openURL(`${twitterAppURL}${this.props.twitterURI}`).catch(() => {
      Linking.openURL(`${twitterBrowserURL}${this.props.twitterURI}`);
    });
  }

  _handleFacebook = () => {
    Linking.openURL(`${facebookAppURL}${this.props.facebookURI}`).catch(() => {
      Linking.openURL(`${facebookBrowserURL}${this.props.facebookURI}`);
    });
  }

  render() {
    const { song } = this.props.navigation.state.params;

    if (!song) return <Loader />;
    return (<Image source={{ uri: song.backgroundImageURL }} style={styles.backgroundImage}>
      <ScrollView ref={scrollView} style={styles.container}>
        <Image source={{ uri: song.titleImageURL }} style={styles.pageTitle} />
        <View style={styles.promoteTitleWrap}>
          <Text style={styles.promoteTitle}>{(`Tweet to hear "${song.title}" in your city!`).toUpperCase()}</Text>
        </View>
        {this.props.showInput ?
          <TextInput
            style={styles.input}
            placeholder={('Type in your city').toUpperCase()}
            placeholderTextColor="rgba(255,255,255,.5)"
            value={this.props.inputTextValue.toUpperCase()}
            onBlur={this._onBlur}
            onFocus={this._onFocus}
            onChangeText={(text) => { this._onChangeText(text); }}
          /> :
          <View style={styles.horizontalBar} />
        }
        {this.props.citySelected &&
          <View style={styles.selectTitle} >
            {!this.props.stationSelected ?
              <Text style={styles.smallText}>
                {('select station near').toUpperCase()}
              </Text> :
              <Text style={styles.smallText}>
                {('share now').toUpperCase()}
              </Text>
            }
            <View style={styles.selectedItem}>
              <Text style={styles.promoteTitle}>
                {this.props.currentSelectedCity.toUpperCase()}
              </Text>

              <TouchableOpacity>
                <Text
                  style={styles.linkText}
                  onPress={() => { this._onChangeCity(); }}
                >
                  CHANGE
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        }
        {this.props.stationSelected &&
          <View>
            <View style={styles.selectedItem}>
              <Text style={styles.promoteTitle}>
                {this.props.currentSelectedStation.toUpperCase()}
              </Text>

              <TouchableOpacity>
                <Text
                  style={styles.linkText}
                  onPress={() => { this._onChangeStation(); }}
                >
                  CHANGE
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        }
        {this.props.showSuggestions &&
          <View>
            <FlatList
              style={{ marginBottom: this.props.keyboardMargin }}
              data={this.props.suggestions}
              keyExtractor={this._keyExtractor}
              renderItem={({ item }) => (
                <TouchableOpacity style={styles.suggestionLink}>
                  <Text
                    style={styles.suggestionText}
                    onPress={() => this._selectCity(item.name, item.lat, item.lon)}
                  >
                    {item.name.toUpperCase()}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        }
        {this.props.showStations &&
          <View>
            <FlatList
              data={this.props.stations}
              keyExtractor={this._keyExtractor}
              renderItem={({ item }) => (
                <TouchableOpacity style={styles.suggestionLink}>
                  <Text
                    style={styles.suggestionText}
                    onPress={() => this._selectStation(
                      item.name,
                      item.cityName,
                      item.twitterHandle
                    )}
                  >{item.name.toUpperCase()}{item.cityName.toUpperCase() !== this.props.currentSelectedCity.toUpperCase() ? ` - ${item.cityName.toUpperCase()}` : ''}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        }
        {this.props.stationSelected &&
          <View style={styles.socialGroup}>
            <TouchableOpacity
              onPress={() => this._handleTweet()}
            >
              <Image
                style={styles.socialIcon}
                source={require('../../assets/icons/Twitter_Social_Icon_White.png')} // eslint-disable-line
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this._handleFacebook()}
            >
              <Image
                style={styles.socialIcon}
                source={require('../../assets/icons/FB-f-Logo__white_50.png')} // eslint-disable-line
              />
            </TouchableOpacity>
          </View>
        }
        <View style={styles.listenBlock}>
          <View style={styles.promoteTitleWrap}>
            <Text style={styles.promoteTitle}>{('listen now').toUpperCase()}</Text>
          </View>
          <View style={styles.appleMusic}>
            <WebView
              source={{ uri: song.applemusicURI }}
              scrollEnabled={false}
            />
          </View>
        </View>
      </ScrollView>
    </Image>
    );
  }
}

SongScreen.propTypes = {
  setTwitterURI: PropTypes.func.isRequired,
  setFacebookURI: PropTypes.func.isRequired,
  selectCity: PropTypes.func.isRequired,
  selectStation: PropTypes.func.isRequired,
  fetchStationsByLatLon: PropTypes.func.isRequired,
  toggleSuggestions: PropTypes.func.isRequired,
  toggleStations: PropTypes.func.isRequired,
  toggleInput: PropTypes.func.isRequired,
  changeText: PropTypes.func.isRequired,
  setKeyboardMargin: PropTypes.func.isRequired,
  inputTextValue: PropTypes.string.isRequired,
  showSuggestions: PropTypes.bool.isRequired,
  showStations: PropTypes.bool.isRequired,
  showInput: PropTypes.bool.isRequired,
  keyboardMargin: PropTypes.number.isRequired,
  navigation: PropTypes.shape().isRequired,
  twitterURI: PropTypes.string.isRequired,
  facebookURI: PropTypes.string.isRequired,
  currentSelectedCity: PropTypes.string.isRequired,
  currentSelectedStation: PropTypes.string.isRequired,
  citySelected: PropTypes.bool.isRequired,
  stationSelected: PropTypes.bool.isRequired,
  stations: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetchStations: PropTypes.func.isRequired,
  resetView: PropTypes.func.isRequired,
  suggestions: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  resetCity: PropTypes.func.isRequired,
  resetStation: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
  inputTextValue: state.view.inputTextValue,
  keyboardMargin: state.view.keyboardMargin,
  showSuggestions: state.view.showSuggestions,
  citySelected: state.view.citySelected,
  stationSelected: state.view.stationSelected,
  suggestions: fromStations.selectCitySuggestions(state),
  showStations: state.view.showStations,
  showInput: state.view.showInput,
  cities: fromStations.selectCityList(state),
  stations: fromStations.selectStationSuggestions(state),
  currentSelectedCity: fromStations.selectSelectedCity(state),
  currentSelectedStation: fromStations.selectSelectedStation(state),
  currentSelectedSong: fromSongs.selectSelectedSongById(state),
  songs: fromSongs.selectSongs(state),
  twitterURI: state.view.twitterURI,
  facebookURI: state.view.facebookURI,
});

const mapDispatchToProps = dispatch => ({
  fetchStationsByLatLon: (lat, lon) => dispatch(fetchStationsByLatLon(lat, lon)),
  changeText: text => dispatch(changeText(text)),
  setKeyboardMargin: margin => dispatch(setKeyboardMargin(margin)),
  fetchStations: () => dispatch(fetchStations()),
  toggleSuggestions: shouldShow => dispatch(toggleSuggestions(shouldShow)),
  toggleStations: shouldShow => dispatch(toggleStations(shouldShow)),
  toggleInput: shouldShow => dispatch(toggleInput(shouldShow)),
  selectCity: city => dispatch(selectCity(city)),
  selectStation: station => dispatch(selectStation(station)),
  selectSong: song => dispatch(selectSong(song)),
  setTwitterURI: uri => dispatch(setTwitterURI(uri)),
  setFacebookURI: uri => dispatch(setFacebookURI(uri)),
  resetView: () => dispatch(resetView()),
  resetCity: () => dispatch(resetCity()),
  resetStation: () => dispatch(resetStation()),
});

SongScreen.navigationOptions = {
  ...navOptions.songScreen,
};

export default connect(mapStateToProps, mapDispatchToProps)(SongScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  promoteTitleWrap: {
    alignItems: 'center',
  },
  pageTitle: {
    height: 160,
    resizeMode: 'contain',
  },
  backgroundImage: {
    flex: 1,
  },
  input: {
    height: 60,
    margin: 8,
    borderColor: 'white',
    color: 'white',
    borderWidth: 4,
    padding: 10,
    fontSize: 14,
  },
  suggestionLink: {
    backgroundColor: 'rgba(255,255,255,.4)',
    marginVertical: 1,
    marginHorizontal: 10,
  },
  suggestionText: {
    padding: 10,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    flexGrow: 1,
    fontSize: 14,
  },
  promoteTitle: {
    width: 240,
    fontSize: 14,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  smallText: {
    fontSize: 10,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  linkText: {
    fontSize: 10,
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent',
    textDecorationLine: 'underline',
  },
  socialIcon: {
    width: 42,
    height: 42,
    margin: 10,
  },
  socialGroup: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  appleMusic: {
    height: 110,
    margin: 10,
  },
  horizontalBar: {
    borderBottomColor: 'white',
    borderBottomWidth: 4,
    width: '80%',
    marginHorizontal: '10%',
    marginVertical: 4,
  },
  selectTitle: {
    marginTop: 10,
  },
  selectedItem: {
    flexDirection: 'row',
  },
  listenBlock: {
    marginTop: 20,
  },
});
