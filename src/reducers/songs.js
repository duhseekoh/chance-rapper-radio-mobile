import { handleActions } from 'redux-actions';
import { normalize, schema } from 'normalizr';
import {
  songsFetchSuccess,
  songsFetchFailure,
  setSelectedSong,
} from '../actions/songs';

const songsSchema = new schema.Entity('songs');
const songsListSchema = [songsSchema];

const initialState = {
  songsById: {},
  error: null,
};

const songs = handleActions({
  [songsFetchSuccess]: (state, { payload }) => {
    const normalized = normalize(payload, songsListSchema);
    return {
      ...state,
      songsById: normalized.entities.songs,
    };
  },

  [songsFetchFailure]: (state, { payload }) => ({
    ...state,
    error: payload,
  }),

  [setSelectedSong]: (state, { payload }) => ({
    ...state,
    currentSelectedSong: payload,
  }),

}, initialState);

export default songs;
