# Quickstart
1. Install Expo XDE: [https://docs.expo.io/versions/v18.0.0/introduction/installation.html](https://docs.expo.io/versions/v18.0.0/introduction/installation.html)
1. Install Expo on your mobile device from the app store
1. Install ios dependencies
  * https://facebook.github.io/react-native/docs/getting-started.html#xcode
1. Install android dependencies
  * https://facebook.github.io/react-native/docs/getting-started.html#android-development-environment
  * I chose to install the android-sdk via homebrew (mentioned in link)
1. Install react native dependencies https://facebook.github.io/react-native/docs/getting-started.html#node-watchman
1. `yarn install`
1. Run it either using one of the following options:
  * Through the expo xde - which gives convenient options for publishing/viewing inside a simulator
  * `yarn start`

# Debugging
1. Press `⌘-D` inside the iOS simulator to open the hidden menu
1. Select Start Remote JS Developing - This will open the remote debugger in a chrome window
1. Alternative, hit cmd-ctrl-z to open the remote js developer console
