/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import navOptions from '../constants/navOptions';
import { fetchSongs } from '../actions/songs';
import * as fromSongs from '../selectors/songs';
import { selectSong, setIsLoading } from '../actions/view';
import Loader from '../components/Loader/Loader';

const scrollView = 'scrollView';

class MainScreen extends Component {
  componentWillMount() {
    this.props.fetchSongs().then((songs) => {
      this._prefetchAssets(songs.payload);
    });
  }

  _prefetchAssets = (songs) => {
    const assetsURIs = (songs.map(song => song.titleImageURL))
      .concat(songs.map(song => song.backgroundImageURL));

    const assets = assetsURIs.map(uri => Image.prefetch(uri));
    return Promise.all(assets)
      .then(() => {
        this._setIsLoading(false);
      });
  };

  _setIsLoading(isLoading) {
    this.props.setIsLoading(isLoading);
  }

  _selectSong(song) {
    this.props.selectSong(song);
  }

  _keyExtractor = (item, index) => index;

  render() {
    const { songs, isLoading } = this.props;

    if (!songs || isLoading) return <Loader />;

    return (
      <ScrollView ref={scrollView} style={styles.container}>
        <View>
          <FlatList
            data={this.props.songs}
            keyExtractor={this._keyExtractor}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.songPreview}
                onPress={() => this.props.goToSongScreen(item)}
              >
                <View>
                  <Image
                    source={{ uri: item.backgroundImageURL }}
                    style={styles.backgroundImage}
                  >
                    <Image source={{ uri: item.titleImageURL }} style={styles.songTitle} />
                  </Image>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>
    );
  }
}

MainScreen.propTypes = {
  fetchSongs: PropTypes.func.isRequired,
  selectSong: PropTypes.func.isRequired,
  goToSongScreen: PropTypes.func.isRequired,
  songs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    backgroundImageURL: PropTypes.string,
    titleImageURL: PropTypes.string,
    videoURL: PropTypes.string,
    tweetText: PropTypes.string,
  })).isRequired,
  isLoading: PropTypes.bool.isRequired,
  setIsLoading: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
  songs: fromSongs.selectSongs(state),
  isLoading: state.view.isLoading,
});

const mapDispatchToProps = dispatch => ({
  fetchSongs: () => dispatch(fetchSongs()),
  selectSong: song => dispatch(selectSong(song)),
  goToSongScreen: item => dispatch(NavigationActions.navigate({ routeName: 'Song', params: { song: item } })),
  setIsLoading: isLoading => dispatch(setIsLoading(isLoading)),
});

MainScreen.navigationOptions = {
  ...navOptions.mainScreen,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#464646',
  },
  songTitle: {
    height: 100,
    resizeMode: 'contain',
  },
  backgroundImage: {
    flex: 1,
  },
  songPreview: {
    flex: 1,
    margin: 10,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
  },
});
