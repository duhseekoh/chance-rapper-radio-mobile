export default {
  primary: {
    text: 'white',
    block: '#2e2e2e',
  },
  flipped: {
    text: '#2e2e2e',
    block: '#f1f1f1',
  },
};
