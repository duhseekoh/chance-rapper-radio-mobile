import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StatusBar, View } from 'react-native';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';

import MainScreen from '../screens/MainScreen';
import SongScreen from '../screens/SongScreen';

export const AppNavigator = StackNavigator({
  Main: { screen: MainScreen },
  Song: { screen: SongScreen },
});

const AppWithStatusBarNavigationState = ({ dispatch, nav }) => (
  <View style={{ flex: 1 }}>
    <StatusBar
      barStyle="light-content"
    />
    <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />

  </View>
);

AppWithStatusBarNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.shape({
    index: PropTypes.number,
    routes: PropTypes.arrayOf(PropTypes.shape()),
  }).isRequired,
};

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithStatusBarNavigationState);
