import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../navigators/AppNavigator';

const firstAction = AppNavigator.router.getActionForPathAndParams('Main');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
const initialNavState = AppNavigator.router.getStateForAction(
  tempNavState
);

const nav = (state = initialNavState, action) => {
  let nextState;
  switch (action.type) {
    case 'Secondary':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Secondary' }),
        state
      );
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  return nextState || state;
};

export default nav;
