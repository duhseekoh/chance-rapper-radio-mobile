import { createSelector } from 'reselect';

export const selectSongsById = state => state.songs.songsById;
export const selectSelectedSongById = state => state.view.currentSelectedSong;
export const selectSongs = state => Object.values(state.songs.songsById) || [];

export const selectSelectedSong = createSelector(
  [selectSelectedSongById, selectSongsById],
  (id, songs) => songs[id] || {},
);
