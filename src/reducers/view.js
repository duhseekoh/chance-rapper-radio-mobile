import { handleActions } from 'redux-actions';

import {
  changeText,
  setKeyboardMargin,
  toggleSuggestions,
  toggleStations,
  toggleInput,
  selectCity,
  selectStation,
  selectSong,
  setTwitterURI,
  setFacebookURI,
  resetView,
  setIsLoading,
  resetCity,
  resetStation,
} from '../actions/view';

const initialState = {
  inputTextValue: '',
  keyboardMargin: 10,
  showSuggestions: false,
  showStations: false,
  showInput: true,
  currentSelectedCity: '',
  citySelected: false,
  stationSelected: false,
  currentSelectedStation: '',
  currentSelectedSong: 0,
  twitterURI: '',
  facebookURI: '',
  isLoading: true,
};

const view = handleActions({
  [setIsLoading]: (state, { payload }) => ({
    ...state,
    isLoading: payload,
  }),

  [changeText]: (state, { payload }) => ({
    ...state,
    inputTextValue: payload,
  }),

  [setKeyboardMargin]: (state, { payload }) => ({
    ...state,
    keyboardMargin: payload,
  }),

  [toggleSuggestions]: (state, { payload }) => ({
    ...state,
    showSuggestions: payload,
  }),

  [toggleStations]: (state, { payload }) => ({
    ...state,
    showStations: payload,
  }),

  [toggleInput]: (state, { payload }) => ({
    ...state,
    showInput: payload,
  }),

  [selectCity]: (state, { payload }) => ({
    ...state,
    currentSelectedCity: payload,
    citySelected: true,
  }),

  [selectStation]: (state, { payload }) => ({
    ...state,
    currentSelectedStation: payload,
    stationSelected: true,
  }),

  [resetCity]: state => ({
    ...state,
    currentSelectedCity: '',
    citySelected: false,
  }),

  [resetStation]: state => ({
    ...state,
    currentSelectedStation: '',
    stationSelected: false,
  }),

  [selectSong]: (state, { payload }) => ({
    ...state,
    currentSelectedSong: payload,
  }),

  [setTwitterURI]: (state, { payload }) => ({
    ...state,
    twitterURI: payload,
  }),

  [setFacebookURI]: (state, { payload }) => ({
    ...state,
    facebookURI: payload,
  }),

  [resetView]: state => ({
    ...state,
    inputTextValue: '',
    keyboardMargin: 10,
    showSuggestions: false,
    showStations: false,
    currentSelectedCity: '',
    currentSelectedStation: '',
    currentSelectedSong: 0,
    twitterURI: '',
    facebookURI: '',
    citySelected: false,
    stationSelected: false,
  }),
}, initialState);

export default view;
