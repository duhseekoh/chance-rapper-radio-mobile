import { API_HOST } from '../../../env-config';

export function apiFetchStations(lat = null, lon = null) { // eslint-disable-line
  const query = (lat && lon) ? `?lat=${lat}&lon=${lon}` : '';
  return fetch(`${API_HOST}/stations${query}`)
    .then(response =>
      (response.ok ?
        response.json() :
        Promise.reject(new Error(`${response.status} Error fetching stations`)))
    );
}
