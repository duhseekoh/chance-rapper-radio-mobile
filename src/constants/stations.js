/* eslint-disable */
const stations = {
  "KS 107.5": {
    "city": "Denver",
    "state": "CO",
    "name": "KS 107.5",
    "twitter": "@KS1075"
  },
    "Skyrock": {
    "city": "Paris",
    "state": "France",
    "name": "Skyrock",
    "twitter": "@SkyrockFM"
  },
  "Generations": {
    "city": "Paris",
    "state": "France",
    "name": "Generations",
    "twitter": "@generations"
  },
  "Z103.5": {
    "city": "Toronto",
    "state": "Canada",
    "name": "Z103.5",
    "twitter": "@Z1035Toronto"
  },
  "Mix 96.1": {
    "city": "San Antonio",
    "state": "TX",
    "name": "Mix 96.1",
    "twitter": "@mix961sa"
  },
  "FM 104": {
    "city": "Dublin",
    "state": "Ireland",
    "name": "FM 104",
    "twitter": "@FM104"
  },
  "KSFM 102.5": {
    "city": "Sacramento",
    "state": "CA",
    "name": "KSFM 102.5",
    "twitter": "@1025Radio"
  },
  "97.9 WIBB": {
    "city": "Macon",
    "state": "GA",
    "name": "97.9 WIBB",
    "twitter": "@979WIBB"
  },
  "95.3 The Gorilla": {
    "city": "Biloxi",
    "state": "MS",
    "name": "95.3 The Gorilla",
    "twitter": "@953gorilla"
  },
  "HD 98.3": {
    "city": "Augusta",
    "state": "GA",
    "name": "HD 98.3",
    "twitter": "@HD983"
  },
  "E93 - WEAS": {
    "city": "Savannah",
    "state": "GA",
    "name": "E93 - WEAS",
    "twitter": "@E93Radio"
  },
  "Star 102.1": {
    "city": "Knoxville",
    "state": "TN",
    "name": "Star 102.1",
    "twitter": "@Star1021"
  },
  "104.5 The Beat": {
    "city": "Orlando",
    "state": "FL",
    "name": "104.5 The Beat",
    "twitter": "@1045thebeat"
  },
  "98.9 Radio Now": {
    "city": "Louisville",
    "state": "KY",
    "name": "98.9 Radio Now",
    "twitter": "@989RadioNow"
  },
  "92.9 The Beat": {
    "city": "Springfield",
    "state": "MO",
    "name": "92.9 The Beat",
    "twitter": "@929TheBeat"
  },
  "103.3 AMP Radio": {
    "city": "Boston",
    "state": "MA",
    "name": "103.3 AMP Radio",
    "twitter": "@1033ampradio"
  },
  "98.5 Virgin": {
    "city": "Calgary ",
    "state": "Canada",
    "name": "98.5 Virgin",
    "twitter": "@VirginRadioYYC"
  },
  "CK 105.5": {
    "city": "Flint",
    "state": "MI",
    "name": "CK 105.5",
    "twitter": "@Ck1055"
  },
  "WPRW (Power 107)": {
    "city": "Augusta",
    "state": "GA",
    "name": "WPRW (Power 107)",
    "twitter": "@Power107augusta"
  },
  "102.7 KIIS-FM": {
    "city": "Los Angeles",
    "state": "CA",
    "name": "102.7 KIIS-FM",
    "twitter": "@1027KIISFM"
  },
  "V-103": {
    "city": "Atlanta",
    "state": "GA",
    "name": "V-103",
    "twitter": "@V103Atlanta"
  },
  "BBC Radio 1": {
    "city": "London",
    "state": "United Kingdom",
    "name": "BBC Radio 1",
    "twitter": "@BBCR1"
  },
  "95.7 The Beat": {
    "city": "Tampa",
    "state": "FL",
    "name": "95.7 The Beat",
    "twitter": "@TheBeatTampa"
  },
  "KHTN (Hot 104.7)": {
    "city": "Modesto",
    "state": "CA",
    "name": "KHTN (Hot 104.7)",
    "twitter": "@Hot1047"
  },
  "97.9 The Beat": {
    "city": "Dallas",
    "state": "TX",
    "name": "97.9 The Beat",
    "twitter": "@979beat"
  },
  "98.5 KRZ": {
    "city": "Wilkes-Barre",
    "state": "PA",
    "name": "98.5 KRZ",
    "twitter": "@985wkrz"
  },
  "Hot 97.5": {
    "city": "Las Vegas",
    "state": "NV",
    "name": "Hot 97.5",
    "twitter": "@HOT975vegas"
  },
  "XL106.7": {
    "city": "Orlando",
    "state": "FL",
    "name": "XL106.7",
    "twitter": "@xl1067"
  },
  "K104.7": {
    "city": "Hudson Valley",
    "state": "NY",
    "name": "K104.7",
    "twitter": "@K1047"
  },
  "Jam FM": {
    "city": "Berlin",
    "state": "Germany",
    "name": "Jam FM",
    "twitter": "@radiojamfm"
  },
  "103.7 KISS FM": {
    "city": "Milwaukee",
    "state": "WI",
    "name": "103.7 KISS FM",
    "twitter": "@1037KISSFM"
  },
  "B93.7": {
    "city": "Greenville",
    "state": "SC",
    "name": "B93.7",
    "twitter": "@B937AlltheHits"
  },
  "KiSS Radio": {
    "city": "Vancouver",
    "state": "Canada",
    "name": "KiSS Radio",
    "twitter": "@kissradio"
  },
  "KDGS (Power 93.5)": {
    "city": "Wichita",
    "state": "KS",
    "name": "KDGS (Power 93.5)",
    "twitter": "@935wichita"
  },
  "107.9 The End": {
    "city": "Sacramento",
    "state": "CA",
    "name": "107.9 The End",
    "twitter": "@1079theend"
  },
  "Energy 94.1": {
    "city": "San Antonio",
    "state": "TX",
    "name": "Energy 94.1",
    "twitter": "@Energy941SA"
  },
  "Wild 104.9": {
    "city": "St. Louis",
    "state": "MO",
    "name": "Wild 104.9",
    "twitter": "@WildSTL"
  },
  "KKWD (Wild 104.9)": {
    "city": "Oklahoma City",
    "state": "OK",
    "name": "KKWD (Wild 104.9)",
    "twitter": "@wild1049"
  },
  "Energy 1023": {
    "city": "Peoria",
    "state": "IL",
    "name": "Energy 1023",
    "twitter": "@Energypeoria"
  },
  "KRRQ": {
    "city": "Lafayette",
    "state": "LA",
    "name": "KRRQ",
    "twitter": "@955krrq"
  },
  "3RRR": {
    "city": "Melbourne",
    "state": "Australia",
    "name": "3RRR",
    "twitter": "@3RRRFM"
  },
  "B95.1": {
    "city": "Macon",
    "state": "GA",
    "name": "B95.1",
    "twitter": "@AllTheHitsB951"
  },
  "Hot 93.5": {
    "city": "Harrisburg",
    "state": "PA",
    "name": "Hot 93.5",
    "twitter": "@HOT935FM"
  },
  "WUBT (101.1 The Beat)": {
    "city": "Nashville",
    "state": "TN",
    "name": "WUBT (101.1 The Beat)",
    "twitter": "@1011thebeat"
  },
  "102.3 The Beat": {
    "city": "Austin",
    "state": "TX",
    "name": "102.3 The Beat",
    "twitter": "@thebeatatx"
  },
  "100.3 Kiss FM": {
    "city": "Greensboro",
    "state": "NC",
    "name": "100.3 Kiss FM",
    "twitter": "@1003kissfm"
  },
  "Channel 96.1": {
    "city": "Charlotte",
    "state": "NC",
    "name": "Channel 96.1",
    "twitter": "@channel961"
  },
  "U92": {
    "city": "Salt Lake City",
    "state": "UT",
    "name": "U92",
    "twitter": "@U92SLC"
  },
  "97 BHT": {
    "city": "Wilkes-Barre",
    "state": "PA",
    "name": "97 BHT",
    "twitter": "@iam97bht"
  },
  "97.9 The Box": {
    "city": "Houston",
    "state": "TX",
    "name": "97.9 The Box",
    "twitter": "@979TheBox"
  },
  "Mix 106.5": {
    "city": "Baltimore",
    "state": "MD",
    "name": "Mix 106.5",
    "twitter": "@Mix1065FM"
  },
  "Q94": {
    "city": "Beaumont",
    "state": "TX",
    "name": "Q94",
    "twitter": "@Q94Music"
  },
  "Hot 95.7": {
    "city": "Houston",
    "state": "TX",
    "name": "Hot 95.7",
    "twitter": "@HOT957"
  },
  "WHXT": {
    "city": "Columbia",
    "state": "SC",
    "name": "WHXT",
    "twitter": "@HOT1039SC"
  },
  "WCDX (iPower 92.1)": {
    "city": "Richmond",
    "state": "VA",
    "name": "WCDX (iPower 92.1)",
    "twitter": "@iPowerRichmond"
  },
  "WJTT (Power 94)": {
    "city": "Chattanooga",
    "state": "TN",
    "name": "WJTT (Power 94)",
    "twitter": "@WJTTPOWER94"
  },
  "102.5 WIOG": {
    "city": "Saginaw",
    "state": "MI",
    "name": "102.5 WIOG",
    "twitter": "@1025WIOG"
  },
  "WEMX (Max 94.1)": {
    "city": "Baton Rouge",
    "state": "LA",
    "name": "WEMX (Max 94.1)",
    "twitter": "@max94one"
  },
  "96.5 KISS FM": {
    "city": "Cleveland",
    "state": "OH",
    "name": "96.5 KISS FM",
    "twitter": "@965KissFM"
  },
  "Hot 107": {
    "city": "Edmonton",
    "state": "Canada",
    "name": "Hot 107",
    "twitter": "@HOT107Edmonton"
  },
  "97.5 NOW": {
    "city": "Lansing",
    "state": "MI",
    "name": "97.5 NOW",
    "twitter": "@975NOW"
  },
  "AC 102.7": {
    "city": "Atlantic City",
    "state": "NJ",
    "name": "AC 102.7",
    "twitter": "@AC102_7"
  },
  "KKND (Power 102.9)": {
    "city": "New Orleans",
    "state": "LA",
    "name": "KKND (Power 102.9)",
    "twitter": "@POWER1029FM"
  },
  "The Heat": {
    "city": "SiriusXM",
    "state": "--",
    "name": "The Heat",
    "twitter": "@SIRIUSXMTHEHEAT"
  },
  "Live 95.5": {
    "city": "Portland",
    "state": "OR",
    "name": "Live 95.5",
    "twitter": "@live955"
  },
  "WBTJ (106.5 The Beat)": {
    "city": "Richmond",
    "state": "VA",
    "name": "WBTJ (106.5 The Beat)",
    "twitter": "@1065thebeat"
  },
  "Radio Fritz": {
    "city": "Berlin",
    "state": "Germany",
    "name": "Radio Fritz",
    "twitter": "@FRITZde"
  },
  "JUMP! 106.9": {
    "city": "Ottawa",
    "state": "Canada",
    "name": "JUMP! 106.9",
    "twitter": "@JumpOttawa"
  },
  "PBS 106.7": {
    "city": "Melbourne",
    "state": "Australia",
    "name": "PBS 106.7",
    "twitter": "@pbsfm"
  },
  "99.7 DJX": {
    "city": "Louisville",
    "state": "KY",
    "name": "99.7 DJX",
    "twitter": "@997DJX"
  },
  "Capital": {
    "city": "Manchester",
    "state": "United Kingdom",
    "name": "Capital",
    "twitter": "@capitalofficial"
  },
  "95.9 Kiss FM": {
    "city": "Appleton",
    "state": "WI",
    "name": "95.9 Kiss FM",
    "twitter": "@Kissfmgreenbay"
  },
  "KISS 95.7": {
    "city": "Hartford",
    "state": "CT",
    "name": "KISS 95.7",
    "twitter": "@kiss957"
  },
  "Z100 Portland": {
    "city": "Portland",
    "state": "OR",
    "name": "Z100 Portland",
    "twitter": "@Z100Portland"
  },
  "Hot 97 (WQHT)": {
    "city": "New York",
    "state": "NY",
    "name": "Hot 97 (WQHT)",
    "twitter": "@HOT97"
  },
  "WOCQ (OC104)": {
    "city": "Salisbury",
    "state": "MD",
    "name": "WOCQ (OC104)",
    "twitter": "@OfficialOc104"
  },
  "98 WJLB": {
    "city": "Detroit",
    "state": "MI",
    "name": "98 WJLB",
    "twitter": "@FM98WJLB"
  },
  "WBLX (93BLX)": {
    "city": "Mobile",
    "state": "AL",
    "name": "WBLX (93BLX)",
    "twitter": "@93BLX"
  },
  "B97 FM": {
    "city": "New Orleans",
    "state": "LA",
    "name": "B97 FM",
    "twitter": "@b97"
  },
  "B104": {
    "city": "Allentown",
    "state": "PA",
    "name": "B104",
    "twitter": "@B104"
  },
  "This Is iRadio": {
    "city": "Dublin",
    "state": "Ireland",
    "name": "This Is iRadio",
    "twitter": "@ThisisiRadio"
  },
  "FBi Radio 94.5": {
    "city": "Sydney",
    "state": "Australia",
    "name": "FBi Radio 94.5",
    "twitter": "@fbiradio"
  },
  "WBTT (105.5 The Beat)": {
    "city": "Ft. Myers",
    "state": "FL",
    "name": "WBTT (105.5 The Beat)",
    "twitter": "@1055thebeat"
  },
  "FLY 92.3": {
    "city": "Albany",
    "state": "NY",
    "name": "FLY 92.3",
    "twitter": "@Fly923"
  },
  "103.3 The Vibe": {
    "city": "Daytona Beach",
    "state": "FL",
    "name": "103.3 The Vibe",
    "twitter": "@WVYB"
  },
  "99.5 WZPL": {
    "city": "Indianapolis",
    "state": "IN",
    "name": "99.5 WZPL",
    "twitter": "@wzpl"
  },
  "Kiss 98.5": {
    "city": "Buffalo",
    "state": "NY",
    "name": "Kiss 98.5",
    "twitter": "@radiostation"
  },
  "NOW Radio": {
    "city": "Edmonton",
    "state": "Canada",
    "name": "NOW Radio",
    "twitter": "@1023nowradio"
  },
  "Hot 89.9 ": {
    "city": "Ottawa",
    "state": "Canada",
    "name": "Hot 89.9 ",
    "twitter": "@newhot899"
  },
  "WGZB (B96.5)": {
    "city": "Louisville",
    "state": "KY",
    "name": "WGZB (B96.5)",
    "twitter": "@TheRealB965"
  },
  "KBOS (B95)": {
    "city": "Fresno",
    "state": "CA",
    "name": "KBOS (B95)",
    "twitter": "@B95Fresno"
  },
  "92.3 AMP": {
    "city": "New York",
    "state": "NY",
    "name": "92.3 AMP",
    "twitter": "@923amp"
  },
  "Jam'n 95.7": {
    "city": "San Diego",
    "state": "CA",
    "name": "Jam'n 95.7",
    "twitter": "@Jamn957"
  },
  "99.9 Virgin Radio": {
    "city": "Toronto",
    "state": "Canada",
    "name": "99.9 Virgin Radio",
    "twitter": "@VirginRadioTO"
  },
  "103.5 KTU (WKTU-FM)": {
    "city": "New York",
    "state": "NY",
    "name": "103.5 KTU (WKTU-FM)",
    "twitter": "@1035KTU"
  },
  "Big FM": {
    "city": "Stuttgart",
    "state": "Germany",
    "name": "Big FM",
    "twitter": "@bigFM"
  },
  "Bob 93.3": {
    "city": "Greenville",
    "state": "NC",
    "name": "Bob 93.3",
    "twitter": "@bob933"
  },
  "103.5 KISS FM": {
    "city": "Chicago",
    "state": "IL",
    "name": "103.5 KISS FM",
    "twitter": "@1035KISSFM"
  },
  "The Beat 98.5": {
    "city": "San Antonio",
    "state": "TX",
    "name": "The Beat 98.5",
    "twitter": "@TheBeat985"
  },
  "93Q": {
    "city": "Syracuse",
    "state": "NY",
    "name": "93Q",
    "twitter": "@93QSyracuse"
  },
  "BBC Radio 1Xtra": {
    "city": "London",
    "state": "United Kingdom",
    "name": "BBC Radio 1Xtra",
    "twitter": "@1Xtra"
  },
  "WCCG 104.5": {
    "city": "Fayetteville",
    "state": "NC",
    "name": "WCCG 104.5",
    "twitter": "@WCCG1045FM"
  },
  "KISS 102.3": {
    "city": "Albany",
    "state": "NY",
    "name": "KISS 102.3",
    "twitter": "@KISS1023ALBANY"
  },
  "WBTF (107.9 The Beat)": {
    "city": "Lexington",
    "state": "KY",
    "name": "WBTF (107.9 The Beat)",
    "twitter": "@thebeat1079lex"
  },
  "KVYB (103.3 The Vibe)": {
    "city": "Oxnard-Ventura",
    "state": "CA",
    "name": "KVYB (103.3 The Vibe)",
    "twitter": "@TheVibe1033"
  },
  "Z90.3 (XHTZ-FM)": {
    "city": "San Diego",
    "state": "CA",
    "name": "Z90.3 (XHTZ-FM)",
    "twitter": "@Z903"
  },
  "KIPR (Power 92)": {
    "city": "Little Rock",
    "state": "AR",
    "name": "KIPR (Power 92)",
    "twitter": "@Power92Jams"
  },
  "104.7 KISS FM": {
    "city": "Phoenix",
    "state": "AZ",
    "name": "104.7 KISS FM",
    "twitter": "@KISSFMPhoenix"
  },
  "EgoFM": {
    "city": "Munich",
    "state": "Germany",
    "name": "EgoFM",
    "twitter": "@egoFM"
  },
  "Q100": {
    "city": "Atlanta",
    "state": "GA",
    "name": "Q100",
    "twitter": "@Q100Atlanta"
  },
  "WAJZ (JAMZ 96.3)": {
    "city": "Albany",
    "state": "NY",
    "name": "WAJZ (JAMZ 96.3)",
    "twitter": "@JAMZ963"
  },
  "104.3 WZYP": {
    "city": "Huntsville",
    "state": "AL",
    "name": "104.3 WZYP",
    "twitter": "@1043WZYP"
  },
  "Live 105.5": {
    "city": "Oxnard-Ventura",
    "state": "CA",
    "name": "Live 105.5",
    "twitter": "@Live1055"
  },
  "Q92": {
    "city": "Gainesville",
    "state": "FL",
    "name": "Q92",
    "twitter": "@allthehitsQ92"
  },
  "Triple J": {
    "city": "Perth",
    "state": "Australia",
    "name": "Triple J",
    "twitter": "@triplej"
  },
  "97.7 WRBJ": {
    "city": "Jackson",
    "state": "MS",
    "name": "97.7 WRBJ",
    "twitter": "@977FM"
  },
  "93.7 KRQ": {
    "city": "Tucson",
    "state": "AZ",
    "name": "93.7 KRQ",
    "twitter": "@KRQQ"
  },
  "93.1 The Party": {
    "city": "Las Vegas",
    "state": "NV",
    "name": "93.1 The Party",
    "twitter": "@931TheParty"
  },
  "Kool 107.3": {
    "city": "Victoria",
    "state": "Canada",
    "name": "Kool 107.3",
    "twitter": "@1073KOOLFM"
  },
  "Go 95.3": {
    "city": "Minneapolis",
    "state": "MN",
    "name": "Go 95.3",
    "twitter": "@Go953mn"
  },
  "WIKS (101.9 Kiss FM)": {
    "city": "Greenville",
    "state": "NC",
    "name": "WIKS (101.9 Kiss FM)",
    "twitter": "@1019online"
  },
  "Sveriges Radio P3": {
    "city": "Stockholm",
    "state": "Sweden",
    "name": "Sveriges Radio P3",
    "twitter": "@SverigesRadioP3"
  },
  "99.3 KISS FM": {
    "city": "Harrisburg",
    "state": "PA",
    "name": "99.3 KISS FM",
    "twitter": "@kissfm993"
  },
  "K94.5": {
    "city": "Shreveport",
    "state": "LA",
    "name": "K94.5",
    "twitter": "@K945"
  },
  "92PRO": {
    "city": "Providence",
    "state": "RI",
    "name": "92PRO",
    "twitter": "@92profm"
  },
  "RTR 92.1": {
    "city": "Perth",
    "state": "Australia",
    "name": "RTR 92.1",
    "twitter": "@RTRFM"
  },
  "Fresh FM": {
    "city": "Adelaide",
    "state": "Australia",
    "name": "Fresh FM",
    "twitter": "@fresh927"
  },
  "95.1 KHOP-FM": {
    "city": "Modesto",
    "state": "CA",
    "name": "95.1 KHOP-FM",
    "twitter": "@KHOP951"
  },
  "105.3 Virgin": {
    "city": "Kitchener",
    "state": "Canada",
    "name": "105.3 Virgin",
    "twitter": "@VirginRadio_KW"
  },
  "KPHW (Power 104.3)": {
    "city": "Honolulu",
    "state": "HI",
    "name": "KPHW (Power 104.3)",
    "twitter": "@power1043"
  },
  "Hot 104.1": {
    "city": "St. Louis",
    "state": "MO",
    "name": "Hot 104.1",
    "twitter": "@Hot1041"
  },
  "99-7 NOW": {
    "city": "San Francisco",
    "state": "CA",
    "name": "99-7 NOW",
    "twitter": "@997now"
  },
  "KHXT (Hot 107.9)": {
    "city": "Lafayette",
    "state": "LA",
    "name": "KHXT (Hot 107.9)",
    "twitter": "@1079isHot"
  },
  "WJMI (99 Jams)": {
    "city": "Jackson",
    "state": "MS",
    "name": "WJMI (99 Jams)",
    "twitter": "@99Jams"
  },
  "104.5 CHUM": {
    "city": "Toronto",
    "state": "Canada",
    "name": "104.5 CHUM",
    "twitter": "@1045CHUMFM"
  },
  "Energy 106": {
    "city": "Winnipeg",
    "state": "Canada",
    "name": "Energy 106",
    "twitter": "@Energy106"
  },
  "Power 93.3": {
    "city": "Seattle",
    "state": "WA",
    "name": "Power 93.3",
    "twitter": "@Power933"
  },
  "Energy 103.7": {
    "city": "San Diego",
    "state": "CA",
    "name": "Energy 103.7",
    "twitter": "@ENERGY1037SD"
  },
  "104.3 HIT-FM": {
    "city": "El Paso",
    "state": "TX",
    "name": "104.3 HIT-FM",
    "twitter": "@1043HitFm"
  },
  "KXHT (Hot 107.1)": {
    "city": "Memphis",
    "state": "TN",
    "name": "KXHT (Hot 107.1)",
    "twitter": "@HOT1071"
  },
  "G105": {
    "city": "Raleigh",
    "state": "NC",
    "name": "G105",
    "twitter": "@G105radio"
  },
  "Fun 107": {
    "city": "Providence",
    "state": "RI",
    "name": "Fun 107",
    "twitter": "@Hot106"
  },
  "KMJJ 99.7": {
    "city": "Shreveport",
    "state": "LA",
    "name": "KMJJ 99.7",
    "twitter": "@KMJJ997"
  },
  "Mix 93.3": {
    "city": "Kansas City",
    "state": "MO",
    "name": "Mix 93.3",
    "twitter": "@Mix933"
  },
  "WBLK": {
    "city": "Buffalo",
    "state": "NY",
    "name": "WBLK",
    "twitter": "@937WBLK"
  },
  "90.3 Amp": {
    "city": "Calgary ",
    "state": "Canada",
    "name": "90.3 Amp",
    "twitter": "@ampcalgary"
  },
  "WIXX 101": {
    "city": "Appleton",
    "state": "WI",
    "name": "WIXX 101",
    "twitter": "@wixx"
  },
  "Q 97.9": {
    "city": "Portland",
    "state": "ME",
    "name": "Q 97.9",
    "twitter": "@Q979"
  },
  "KUBT (93.9 The Beat)": {
    "city": "Honolulu",
    "state": "HI",
    "name": "KUBT (93.9 The Beat)",
    "twitter": "@939beat"
  },
  "Kiss 92.5": {
    "city": "Toronto",
    "state": "Canada",
    "name": "Kiss 92.5",
    "twitter": "@KiSS925"
  },
  "97.3 NOW": {
    "city": "Milwaukee",
    "state": "WI",
    "name": "97.3 NOW",
    "twitter": "@973NOW"
  },
  "KISS 106.7": {
    "city": "Rochester",
    "state": "NY",
    "name": "KISS 106.7",
    "twitter": "@KISSRochester"
  },
  "KUBE 104.9": {
    "city": "Seattle",
    "state": "WA",
    "name": "KUBE 104.9",
    "twitter": "@KUBE1049Tacoma"
  },
  "Power 95.3": {
    "city": "Orlando",
    "state": "FL",
    "name": "Power 95.3",
    "twitter": "@POWER953"
  },
  "KKUU (U-92.7)": {
    "city": "Palm Springs",
    "state": "CA",
    "name": "KKUU (U-92.7)",
    "twitter": "@U927"
  },
  "95sx": {
    "city": "Charleston",
    "state": "SC",
    "name": "95sx",
    "twitter": "@95sxHitMusicNow"
  },
  "KWIN": {
    "city": "Stockton",
    "state": "CA",
    "name": "KWIN",
    "twitter": "@KWINradio"
  },
  "WKHT (Hot 104.5)": {
    "city": "Knoxville",
    "state": "TN",
    "name": "WKHT (Hot 104.5)",
    "twitter": "@HOT1045"
  },
  "KWYL (Wild 102.9)": {
    "city": "Reno",
    "state": "NV",
    "name": "KWYL (Wild 102.9)",
    "twitter": "@wild1029"
  },
  "Sirius HITS-1": {
    "city": "SiriusXM",
    "state": "--",
    "name": "Sirius HITS-1",
    "twitter": "@SiriusXMHits1"
  },
  "95.1 WAYV": {
    "city": "Atlantic City",
    "state": "NJ",
    "name": "95.1 WAYV",
    "twitter": "@951WAYV"
  },
  "JAM'N 107.5": {
    "city": "Portland",
    "state": "OR",
    "name": "JAM'N 107.5",
    "twitter": "@JAMN1075"
  },
  "95.1 WAPE": {
    "city": "Jacksonville",
    "state": "FL",
    "name": "95.1 WAPE",
    "twitter": "@951wape"
  },
  "KDON (102.5 KDON)": {
    "city": "Monterey-Salinas",
    "state": "CA",
    "name": "KDON (102.5 KDON)",
    "twitter": "@1025KDON"
  },
  "Z95.3": {
    "city": "Vancouver",
    "state": "Canada",
    "name": "Z95.3",
    "twitter": "@Z953VAN"
  },
  "Electric 94.9": {
    "city": "Johnson City",
    "state": "TN",
    "name": "Electric 94.9",
    "twitter": "@electric949"
  },
  "WZHT Hot 105.7": {
    "city": "Montgomery",
    "state": "AL",
    "name": "WZHT Hot 105.7",
    "twitter": "@Hot1057"
  },
  "Shade 45": {
    "city": "SiriusXM",
    "state": "--",
    "name": "Shade 45",
    "twitter": "@Shade45"
  },
  "Power 96 (WPOW-FM)": {
    "city": "Miami",
    "state": "FL",
    "name": "Power 96 (WPOW-FM)",
    "twitter": "@Power965"
  },
  "103.5 The Beat": {
    "city": "Miami",
    "state": "FL",
    "name": "103.5 The Beat",
    "twitter": "@1035TheBEAT"
  },
  "Fresh 92.5": {
    "city": "Edmonton",
    "state": "Canada",
    "name": "Fresh 92.5",
    "twitter": "@925FreshRadio"
  },
  "Power 107.5": {
    "city": "Columbus",
    "state": "OH",
    "name": "Power 107.5",
    "twitter": "@Power1075"
  },
  "NRJ": {
    "city": "Paris",
    "state": "France",
    "name": "NRJ",
    "twitter": "@NRJhitmusiconly"
  },
  "Power 105.1": {
    "city": "New York",
    "state": "NY",
    "name": "Power 105.1",
    "twitter": "@Power1051"
  },
  "KKSS (Kiss 97.3)": {
    "city": "Albuquerque",
    "state": "NM",
    "name": "KKSS (Kiss 97.3)",
    "twitter": "@mykiss973"
  },
  "Planet 96.7 Jams": {
    "city": "Burlington",
    "state": "VT",
    "name": "Planet 96.7 Jams",
    "twitter": "@Planet967"
  },
  "WRCL (Club 93.7)": {
    "city": "Flint",
    "state": "MI",
    "name": "WRCL (Club 93.7)",
    "twitter": "@FlintsClub937"
  },
  "WJMH (102 JAMZ)": {
    "city": "Greensboro",
    "state": "NC",
    "name": "WJMH (102 JAMZ)",
    "twitter": "@102_JAMZ"
  },
  "KISS FM": {
    "city": "Melbourne",
    "state": "Australia",
    "name": "KISS FM",
    "twitter": "@KISSFMAustralia"
  },
  "977 KRCK": {
    "city": "Palm Springs",
    "state": "CA",
    "name": "977 KRCK",
    "twitter": "@977krck"
  },
  "KDDB (102.7 Da Bomb)": {
    "city": "Honolulu",
    "state": "HI",
    "name": "KDDB (102.7 Da Bomb)",
    "twitter": "@dabomb1027"
  },
  "97.1 AMP Radio": {
    "city": "Los Angeles",
    "state": "CA",
    "name": "97.1 AMP Radio",
    "twitter": "@971AMPRadio"
  },
  "Hot 96.3": {
    "city": "Indianapolis",
    "state": "IN",
    "name": "Hot 96.3",
    "twitter": "@Hot963"
  },
  "Hot 100.5": {
    "city": "Norfolk",
    "state": "VA",
    "name": "Hot 100.5",
    "twitter": "@HOThits1005"
  },
  "Beat 92.5": {
    "city": "Montreal",
    "state": "Canada",
    "name": "Beat 92.5",
    "twitter": "@thebeat925"
  },
  "104.7 WNOK": {
    "city": "Columbia",
    "state": "SC",
    "name": "104.7 WNOK",
    "twitter": "@1047WNOK"
  },
  "B100": {
    "city": "Quad Cities",
    "state": "IA-IL",
    "name": "B100",
    "twitter": "@B100QC"
  },
  "Q1075": {
    "city": "Memphis",
    "state": "TN",
    "name": "Q1075",
    "twitter": "@Q1075"
  },
  "MOUV": {
    "city": "Paris",
    "state": "France",
    "name": "MOUV",
    "twitter": "@mouv"
  },
  "FM97": {
    "city": "Lancaster",
    "state": "PA",
    "name": "FM97",
    "twitter": "@FM97"
  },
  "2SER 107.3": {
    "city": "Sydney",
    "state": "Australia",
    "name": "2SER 107.3",
    "twitter": "@2ser"
  },
  "POWER 105.7": {
    "city": "Fayetteville",
    "state": "AR",
    "name": "POWER 105.7",
    "twitter": "@POWER1057"
  },
  "WEUP": {
    "city": "Huntsville",
    "state": "AL",
    "name": "WEUP",
    "twitter": "@1031WEUP"
  },
  "WLZN (Blazin' 92.3)": {
    "city": "Macon",
    "state": "GA",
    "name": "WLZN (Blazin' 92.3)",
    "twitter": "@BLAZIN923MACON"
  },
  "KissFM UK": {
    "city": "Glasglow",
    "state": "United Kingdom",
    "name": "KissFM UK",
    "twitter": "@KissFMUK"
  },
  "WZPW": {
    "city": "Peoria",
    "state": "IL",
    "name": "WZPW",
    "twitter": "@Peorias923"
  },
  "95 TripleX": {
    "city": "Burlington",
    "state": "VT",
    "name": "95 TripleX",
    "twitter": "@95TripleX"
  },
  "Hits 93.1": {
    "city": "Bakersfield",
    "state": "CA",
    "name": "Hits 93.1",
    "twitter": "@hits931fm"
  },
  "Z100": {
    "city": "New York",
    "state": "NY",
    "name": "Z100",
    "twitter": "@Z100NewYork"
  },
  "Z107": {
    "city": "Portsmouth",
    "state": "NH",
    "name": "Z107",
    "twitter": "@Z107FM"
  },
  "Z104": {
    "city": "Madison",
    "state": "WI",
    "name": "Z104",
    "twitter": "@Z104"
  },
  "Channel 99.9": {
    "city": "Dayton",
    "state": "OH",
    "name": "Channel 99.9",
    "twitter": "@Channel999"
  },
  "104.3 NOW FM": {
    "city": "Las Vegas",
    "state": "NV",
    "name": "104.3 NOW FM",
    "twitter": "@1043NOW"
  },
  "Hot 93.3 Hits": {
    "city": "Dallas",
    "state": "TX",
    "name": "Hot 93.3 Hits",
    "twitter": "@hot933hits"
  },
  "Pulse FM": {
    "city": "Raleigh",
    "state": "NC",
    "name": "Pulse FM",
    "twitter": "@TheNewPulseFM"
  },
  "Y102": {
    "city": "Montgomery",
    "state": "AL",
    "name": "Y102",
    "twitter": "@y102"
  },
  "KJMM": {
    "city": "Tulsa",
    "state": "OK",
    "name": "KJMM",
    "twitter": "@105KJAMZ"
  },
  "Y100": {
    "city": "Miami",
    "state": "FL",
    "name": "Y100",
    "twitter": "@Y100MIAMI"
  },
  "Mix 105.1": {
    "city": "Salt Lake City",
    "state": "UT",
    "name": "Mix 105.1",
    "twitter": "@MIX1051Utah"
  },
  "Hits 97.3": {
    "city": "Miami",
    "state": "FL",
    "name": "Hits 97.3",
    "twitter": "@HITS973"
  },
  "104.7 KDUK": {
    "city": "Eugene",
    "state": "OR",
    "name": "104.7 KDUK",
    "twitter": "@1047KDUK"
  },
  "Hot 107.9": {
    "city": "Ft. Wayne",
    "state": "IN",
    "name": "Hot 107.9",
    "twitter": "@HOT1079"
  },
  "Radio NOW 100.9": {
    "city": "Indianapolis",
    "state": "IN",
    "name": "Radio NOW 100.9",
    "twitter": "@RadioNOW1009"
  },
  "104.5 WSNX": {
    "city": "Grand Rapids",
    "state": "MI",
    "name": "104.5 WSNX",
    "twitter": "@1045SNX"
  },
  "KCAQ (Q104.7)": {
    "city": "Oxnard-Ventura",
    "state": "CA",
    "name": "KCAQ (Q104.7)",
    "twitter": "@Q1047"
  },
  "Z106.3": {
    "city": "Albuquerque",
    "state": "NM",
    "name": "Z106.3",
    "twitter": "@Z1063"
  },
  "WDHT (Hot 102.9)": {
    "city": "Dayton",
    "state": "OH",
    "name": "WDHT (Hot 102.9)",
    "twitter": "@HOT1029"
  },
  "Energy 95.3": {
    "city": "Bakersfield",
    "state": "CA",
    "name": "Energy 95.3",
    "twitter": "@energy953"
  },
  "Z104.3": {
    "city": "Baltimore",
    "state": "MD",
    "name": "Z104.3",
    "twitter": "@Z1043"
  },
  "107.5 WGCI": {
    "city": "Chicago",
    "state": "IL",
    "name": "107.5 WGCI",
    "twitter": "@WGCI"
  },
  "98.5 Kiss FM": {
    "city": "Peoria",
    "state": "IL",
    "name": "98.5 Kiss FM",
    "twitter": "@Kisspeoria"
  },
  "KJ103": {
    "city": "Oklahoma City",
    "state": "OK",
    "name": "KJ103",
    "twitter": "@1027kj103"
  },
  "97.3 Kiss FM": {
    "city": "Savannah",
    "state": "GA",
    "name": "97.3 Kiss FM",
    "twitter": "@973Kissfm"
  },
  "Nova Radio": {
    "city": "Paris",
    "state": "France",
    "name": "Nova Radio",
    "twitter": "@laRadioNova"
  },
  "WHRK (K97)": {
    "city": "Memphis",
    "state": "TN",
    "name": "WHRK (K97)",
    "twitter": "@K97FM"
  },
  "B96.3": {
    "city": "Chicago",
    "state": "IL",
    "name": "B96.3",
    "twitter": "@B96Chicago"
  },
  "Kiss 107": {
    "city": "Cincinnati",
    "state": "OH",
    "name": "Kiss 107",
    "twitter": "@KISS107"
  },
  "Z 107.9": {
    "city": "Cleveland",
    "state": "OH",
    "name": "Z 107.9",
    "twitter": "@Z1079"
  },
  "WFXA (Foxie 103 JAMZ)": {
    "city": "Augusta",
    "state": "GA",
    "name": "WFXA (Foxie 103 JAMZ)",
    "twitter": "@WFXAFM"
  },
  "94.5 Virgin": {
    "city": "Vancouver",
    "state": "Canada",
    "name": "94.5 Virgin",
    "twitter": "@VirginRadioVan"
  },
  "96.7 KISS": {
    "city": "Austin",
    "state": "TX",
    "name": "96.7 KISS",
    "twitter": "@967kissfm"
  },
  "NOW 96.3": {
    "city": "St. Louis",
    "state": "MO",
    "name": "NOW 96.3",
    "twitter": "@now963stl"
  },
  "106 KMEL": {
    "city": "San Francisco",
    "state": "CA",
    "name": "106 KMEL",
    "twitter": "@106KMEL"
  },
  "B98.5": {
    "city": "Monmouth-Ocean",
    "state": "NJ",
    "name": "B98.5",
    "twitter": "@TheB985"
  },
  "WWWZ (Z-93 Jamz)": {
    "city": "Charleston",
    "state": "SC",
    "name": "WWWZ (Z-93 Jamz)",
    "twitter": "@Z93SC"
  },
  "93.7 The Beat": {
    "city": "Houston",
    "state": "TX",
    "name": "93.7 The Beat",
    "twitter": "@937thebeat"
  },
  "107.1 The Monkey": {
    "city": "Biloxi",
    "state": "MS",
    "name": "107.1 The Monkey",
    "twitter": "@1071themonkey"
  },
  "103.7 The Q": {
    "city": "Birmingham",
    "state": "AL",
    "name": "103.7 The Q",
    "twitter": "@1037theq"
  },
  "WJBT (93.3 The Beat)": {
    "city": "Jacksonville",
    "state": "FL",
    "name": "WJBT (93.3 The Beat)",
    "twitter": "@933thebeatjamz"
  },
  "99 Jamz": {
    "city": "Miami",
    "state": "FL",
    "name": "99 Jamz",
    "twitter": "@99JAMZ"
  },
  "HOT 99.5": {
    "city": "Washington",
    "state": "DC",
    "name": "HOT 99.5",
    "twitter": "@hot995"
  },
  "Live 101.5": {
    "city": "Phoenix",
    "state": "AZ",
    "name": "Live 101.5",
    "twitter": "@live1015phoenix"
  },
  "WQHH (Power 96.5)": {
    "city": "Lansing",
    "state": "MI",
    "name": "WQHH (Power 96.5)",
    "twitter": "@WQHH965"
  },
  "WOWI (103 JAMZ)": {
    "city": "Norfolk",
    "state": "VA",
    "name": "WOWI (103 JAMZ)",
    "twitter": "@103JAMZRADIO"
  },
  "96.1 KISS": {
    "city": "Pittsburgh",
    "state": "PA",
    "name": "96.1 KISS",
    "twitter": "@961KISS"
  },
  "Alice 107.7": {
    "city": "Little Rock",
    "state": "AR",
    "name": "Alice 107.7",
    "twitter": "@Alice1077radio"
  },
  "HITZ 104.9": {
    "city": "Visalia-Tulare",
    "state": "CA",
    "name": "HITZ 104.9",
    "twitter": "@Hitz1049"
  },
  "107.5 KISS FM": {
    "city": "Des Moines",
    "state": "IA",
    "name": "107.5 KISS FM",
    "twitter": "@KISS1075"
  },
  "KOPW (Power 106.9)": {
    "city": "Omaha",
    "state": "NE",
    "name": "KOPW (Power 106.9)",
    "twitter": "@power1069"
  },
  "103.1 Virgin ": {
    "city": "Winnipeg",
    "state": "Canada",
    "name": "103.1 Virgin ",
    "twitter": "@VirginRadioWPG"
  },
  "91.7 Bounce": {
    "city": "Edmonton",
    "state": "Canada",
    "name": "91.7 Bounce",
    "twitter": "@917thebounce"
  },
  "WLTO (Hot 102)": {
    "city": "Lexington",
    "state": "KY",
    "name": "WLTO (Hot 102)",
    "twitter": "@HOT102lex"
  },
  "97.1 ZHT": {
    "city": "Salt Lake City",
    "state": "UT",
    "name": "97.1 ZHT",
    "twitter": "@971zht"
  },
  "Wild 94.1": {
    "city": "Tampa",
    "state": "FL",
    "name": "Wild 94.1",
    "twitter": "@WiLD941"
  },
  "KISS 108": {
    "city": "Boston",
    "state": "MA",
    "name": "KISS 108",
    "twitter": "@Kiss108"
  },
  "Wild 94.9": {
    "city": "San Francisco",
    "state": "CA",
    "name": "Wild 94.9",
    "twitter": "@Wild949"
  },
  "B103.9": {
    "city": "Ft. Myers",
    "state": "FL",
    "name": "B103.9",
    "twitter": "@B1039Radio"
  },
  "Radio Adelaide": {
    "city": "Adelaide",
    "state": "Australia",
    "name": "Radio Adelaide",
    "twitter": "@radioadelaide"
  },
  "Kool 101.5": {
    "city": "Calgary ",
    "state": "Canada",
    "name": "Kool 101.5",
    "twitter": "@1015KooLFM"
  },
  "101.9 AMP Radio": {
    "city": "Orlando",
    "state": "FL",
    "name": "101.9 AMP Radio",
    "twitter": "@1019ampradio"
  },
  "Power 106": {
    "city": "Los Angeles",
    "state": "CA",
    "name": "Power 106",
    "twitter": "@Power106LA"
  },
  "105.3 KiSS": {
    "city": "Ottawa",
    "state": "Canada",
    "name": "105.3 KiSS",
    "twitter": "@kissottawa"
  },
  "KOHT (Hot 98.3)": {
    "city": "Tucson",
    "state": "AZ",
    "name": "KOHT (Hot 98.3)",
    "twitter": "@Hot983"
  },
  "99.1 KGGI": {
    "city": "Riverside",
    "state": "CA",
    "name": "99.1 KGGI",
    "twitter": "@991KGGI"
  },
  "101.3 Virgin": {
    "city": "Halifax",
    "state": "Canada",
    "name": "101.3 Virgin",
    "twitter": "@VirginRadioHali"
  },
  "WHTP": {
    "city": "Portland",
    "state": "ME",
    "name": "WHTP",
    "twitter": "@Hot1047Maine"
  },
  "103.1 KiSS": {
    "city": "Victoria",
    "state": "Canada",
    "name": "103.1 KiSS",
    "twitter": "@kiss1031"
  },
  "KEZE (Hot 96.9)": {
    "city": "Spokane",
    "state": "WA",
    "name": "KEZE (Hot 96.9)",
    "twitter": "@Hot969"
  },
  "Z107-7": {
    "city": "St. Louis",
    "state": "MO",
    "name": "Z107-7",
    "twitter": "@z1077"
  },
  "95.7 The VIBE": {
    "city": "Kansas City",
    "state": "MO",
    "name": "95.7 The VIBE",
    "twitter": "@957TheVibe"
  },
  "107.5 KZL": {
    "city": "Greensboro",
    "state": "NC",
    "name": "107.5 KZL",
    "twitter": "@1075KZL"
  },
  "105.3 HOT FM": {
    "city": "Grand Rapids",
    "state": "MI",
    "name": "105.3 HOT FM",
    "twitter": "@KISS105_3"
  },
  "KAGM (Wild 106.7)": {
    "city": "Albuquerque",
    "state": "NM",
    "name": "KAGM (Wild 106.7)",
    "twitter": "@wild1067abq"
  },
  "KISV (Hot 94.1)": {
    "city": "Bakersfield",
    "state": "CA",
    "name": "KISV (Hot 94.1)",
    "twitter": "@hot941"
  },
  "Planet 102.3": {
    "city": "Corpus Christi",
    "state": "TX",
    "name": "Planet 102.3",
    "twitter": "@Planet1023"
  },
  "WJUC (The Juice)": {
    "city": "Toledo",
    "state": "OH",
    "name": "WJUC (The Juice)",
    "twitter": "@THEJUICE1073"
  },
  "96.5 AMP Radio": {
    "city": "Philadelphia",
    "state": "PA",
    "name": "96.5 AMP Radio",
    "twitter": "@965ampradio"
  },
  "92 Q": {
    "city": "Baltimore",
    "state": "MD",
    "name": "92 Q",
    "twitter": "@92QJamsBmore"
  },
  "Kiss 105.3": {
    "city": "Gainesville",
    "state": "FL",
    "name": "Kiss 105.3",
    "twitter": "@KISS105_3"
  },
  "96.1 KISS FM": {
    "city": "Ft. Collins",
    "state": "CO",
    "name": "96.1 KISS FM",
    "twitter": "@961kissfm"
  },
  "104.9 Virgin": {
    "city": "Edmonton",
    "state": "Canada",
    "name": "104.9 Virgin",
    "twitter": "@VirginRadioYEG"
  },
  "Capital XTRA": {
    "city": "London",
    "state": "United Kingdom",
    "name": "Capital XTRA",
    "twitter": "@CapitalXTRA"
  },
  "95.9 KiSS": {
    "city": "Calgary ",
    "state": "Canada",
    "name": "95.9 KiSS",
    "twitter": "@kiss959calgary"
  },
  "N-Joy": {
    "city": "Northern Germany",
    "state": "Germany",
    "name": "N-Joy",
    "twitter": "@NJOYDE"
  },
  "KC101": {
    "city": "New Haven",
    "state": "CT",
    "name": "KC101",
    "twitter": "@KC1013"
  },
  "KZFM (Z95)": {
    "city": "Corpus Christi",
    "state": "TX",
    "name": "KZFM (Z95)",
    "twitter": "@hotz95"
  },
  "95.7 The Party": {
    "city": "Denver",
    "state": "CO",
    "name": "95.7 The Party",
    "twitter": "@957theparty"
  },
  "DRadio Wissen": {
    "city": "Cologne",
    "state": "Germany",
    "name": "DRadio Wissen",
    "twitter": "@DRadioWissen"
  },
  "97.9 KISS FM": {
    "city": "Jacksonville",
    "state": "FL",
    "name": "97.9 KISS FM",
    "twitter": "@979KISSFM"
  },
  "92.1 The Beat": {
    "city": "Tulsa",
    "state": "OK",
    "name": "92.1 The Beat",
    "twitter": "@921thebeat"
  },
  "FunX": {
    "city": "Rotterdam",
    "state": "Netherlands",
    "name": "FunX",
    "twitter": "@FunX"
  },
  "Power 92.3": {
    "city": "Chicago",
    "state": "IL",
    "name": "Power 92.3",
    "twitter": "@Power92Chicago"
  },
  "104.1 KRBE": {
    "city": "Houston",
    "state": "TX",
    "name": "104.1 KRBE",
    "twitter": "@krbe"
  },
  "The Voice": {
    "city": "Stockholm",
    "state": "Sweden",
    "name": "The Voice",
    "twitter": "@TheVoiceRadioTV"
  },
  "Wild 95.5": {
    "city": "West Palm",
    "state": "FL",
    "name": "Wild 95.5",
    "twitter": "@WiLD955"
  },
  "WQUE (Q93)": {
    "city": "New Orleans",
    "state": "LA",
    "name": "WQUE (Q93)",
    "twitter": "@Q93FM"
  },
  "Beat 91.5": {
    "city": "Kitchener",
    "state": "Canada",
    "name": "Beat 91.5",
    "twitter": "@915theBeat"
  },
  "98.7 AMP Radio": {
    "city": "Detroit",
    "state": "MI",
    "name": "98.7 AMP Radio",
    "twitter": "@987ampradio"
  },
  "JAM'N 94.5": {
    "city": "Boston",
    "state": "MA",
    "name": "JAM'N 94.5",
    "twitter": "@JAMN945"
  },
  "101.1 The Wiz": {
    "city": "Cincinnati",
    "state": "OH",
    "name": "101.1 The Wiz",
    "twitter": "@wiznationcincy"
  },
  "K97.5": {
    "city": "Raleigh",
    "state": "NC",
    "name": "K97.5",
    "twitter": "@k975"
  },
  "WKYS": {
    "city": "Washington",
    "state": "DC",
    "name": "WKYS",
    "twitter": "@939WKYS"
  },
  "Channel 96.3": {
    "city": "Wichita",
    "state": "KS",
    "name": "Channel 96.3",
    "twitter": "@Channel963"
  },
  "WBHJ (95.7 Jamz)": {
    "city": "Birmingham",
    "state": "AL",
    "name": "WBHJ (95.7 Jamz)",
    "twitter": "@957Jamz"
  },
  "K HITS 106.9": {
    "city": "Tulsa",
    "state": "OK",
    "name": "K HITS 106.9",
    "twitter": "@khitstulsa"
  },
  "V-100.7 Jams": {
    "city": "Milwaukee",
    "state": "WI",
    "name": "V-100.7 Jams",
    "twitter": "@V1007"
  },
  "101.3 KDWB": {
    "city": "Minneapolis",
    "state": "MN",
    "name": "101.3 KDWB",
    "twitter": "@1013KDWB"
  },
  "Q98": {
    "city": "Fayetteville",
    "state": "NC",
    "name": "Q98",
    "twitter": "@Q98Fayetteville"
  },
  "Power 96.1": {
    "city": "Atlanta",
    "state": "GA",
    "name": "Power 96.1",
    "twitter": "@POWERATL"
  },
  "Power 96.5": {
    "city": "Springfield",
    "state": "MO",
    "name": "Power 96.5",
    "twitter": "@power965fm"
  },
  "94.1 The Beat - WQBT": {
    "city": "Savannah",
    "state": "GA",
    "name": "94.1 The Beat - WQBT",
    "twitter": "@941thebeat"
  },
  "WZMX (Hot 93.7)": {
    "city": "Hartford",
    "state": "CT",
    "name": "WZMX (Hot 93.7)",
    "twitter": "@hot937"
  },
  "106.1 BLI": {
    "city": "Nassau-Suffolk",
    "state": "NY",
    "name": "106.1 BLI",
    "twitter": "@1061BLI"
  },
  "106.1 KISS FM": {
    "city": "Dallas",
    "state": "TX",
    "name": "106.1 KISS FM",
    "twitter": "@1061KISSFMDFW"
  },
  "93.3 KOB-FM": {
    "city": "Albuquerque",
    "state": "NM",
    "name": "93.3 KOB-FM",
    "twitter": "@kobfm"
  },
  "i106 Hits": {
    "city": "Nashville",
    "state": "TN",
    "name": "i106 Hits",
    "twitter": "@i106fm"
  },
  "C100": {
    "city": "Halifax",
    "state": "Canada",
    "name": "C100",
    "twitter": "@C100FM"
  },
  "WHOT-FM": {
    "city": "Youngstown",
    "state": "OH",
    "name": "WHOT-FM",
    "twitter": "@Hot101FM"
  },
  "Kiss 95.1": {
    "city": "Charlotte",
    "state": "NC",
    "name": "Kiss 95.1",
    "twitter": "@Kiss951WNKS"
  },
  "Virgin96": {
    "city": "Montreal",
    "state": "Canada",
    "name": "Virgin96",
    "twitter": "@VirginRadioMTL"
  },
  "WZFX (Foxy 99)": {
    "city": "Fayetteville",
    "state": "NC",
    "name": "WZFX (Foxy 99)",
    "twitter": "@Foxy991"
  },
  "Power 98": {
    "city": "Charlotte",
    "state": "NC",
    "name": "Power 98",
    "twitter": "@Power98FM"
  },
  "Power 99": {
    "city": "Philadelphia",
    "state": "PA",
    "name": "Power 99",
    "twitter": "@Power99Philly"
  },
  "Fresh 95.3": {
    "city": "Hamilton",
    "state": "Canada",
    "name": "Fresh 95.3",
    "twitter": "@953FreshRadio"
  },
  "KVSP (Power 103.5)": {
    "city": "Oklahoma City",
    "state": "OK",
    "name": "KVSP (Power 103.5)",
    "twitter": "@power1035"
  },
  "94.5 PST": {
    "city": "Trenton",
    "state": "NJ",
    "name": "94.5 PST",
    "twitter": "@945pst"
  },
  "KBTT 103.7 Tha Beat": {
    "city": "Shreveport",
    "state": "LA",
    "name": "KBTT 103.7 Tha Beat",
    "twitter": "@1037ThaBeat"
  },
  "K104": {
    "city": "Dallas",
    "state": "TX",
    "name": "K104",
    "twitter": "@K104FM"
  },
  "Real 92.3": {
    "city": "Los Angeles",
    "state": "CA",
    "name": "Real 92.3",
    "twitter": "@Real923LA"
  },
  "WHZT (Hot 98.1)": {
    "city": "Greenville",
    "state": "SC",
    "name": "WHZT (Hot 98.1)",
    "twitter": "@Hot981"
  },
  "KissFM": {
    "city": "Berlin",
    "state": "Germany",
    "name": "KissFM",
    "twitter": "@988KISSFM"
  },
  "Energy 106.9": {
    "city": "Milwaukee",
    "state": "WI",
    "name": "Energy 106.9",
    "twitter": "@ENERGY1069"
  },
  "Hip-Hop Nation": {
    "city": "SiriusXM",
    "state": "--",
    "name": "Hip-Hop Nation",
    "twitter": "@HipHopNation"
  },
  "Hot 103.5": {
    "city": "Sacramento",
    "state": "CA",
    "name": "Hot 103.5",
    "twitter": "@Hot_1035"
  },
  "107.9 Mix FM": {
    "city": "McAllen",
    "state": "TX",
    "name": "107.9 Mix FM",
    "twitter": "@1079mixfm"
  },
  "Hot 105.7": {
    "city": "San Jose",
    "state": "CA",
    "name": "Hot 105.7",
    "twitter": "@hot1057fm"
  },
  "KWYD (Wild 101.1)": {
    "city": "Boise",
    "state": "ID",
    "name": "KWYD (Wild 101.1)",
    "twitter": "@wild101"
  },
  "Hot 107.5": {
    "city": "Detroit",
    "state": "MI",
    "name": "Hot 107.5",
    "twitter": "@hiphopdetroit"
  },
  "K92": {
    "city": "Roanoke",
    "state": "VA",
    "name": "K92",
    "twitter": "@K92radio"
  },
  "WE 96.3": {
    "city": "Portland",
    "state": "OR",
    "name": "WE 96.3",
    "twitter": "@we963pdx"
  },
  "Hot 103 Jamz": {
    "city": "Kansas City",
    "state": "MO",
    "name": "Hot 103 Jamz",
    "twitter": "@Hot103Jamz"
  },
  "WWKX (Hot 106.3)": {
    "city": "Providence",
    "state": "RI",
    "name": "WWKX (Hot 106.3)",
    "twitter": "@Hot106"
  },
  "98.5 KLUC": {
    "city": "Las Vegas",
    "state": "NV",
    "name": "98.5 KLUC",
    "twitter": "@985KLUC"
  },
  "Q 102": {
    "city": "Philadelphia",
    "state": "PA",
    "name": "Q 102",
    "twitter": "@Q102Philly"
  },
  "WJQM (93.1 JAMZ)": {
    "city": "Madison",
    "state": "WI",
    "name": "WJQM (93.1 JAMZ)",
    "twitter": "@931Jamz"
  },
  "97.5 WABD": {
    "city": "Mobile",
    "state": "AL",
    "name": "97.5 WABD",
    "twitter": "@975wabd"
  },
  "WTMG (Magic 101.3)": {
    "city": "Gainesville",
    "state": "FL",
    "name": "WTMG (Magic 101.3)",
    "twitter": "@Magic1013FM"
  },
  "WNCI 97.9": {
    "city": "Columbus",
    "state": "OH",
    "name": "WNCI 97.9",
    "twitter": "@979WNCI"
  },
  "KSEQ (Q 97.1)": {
    "city": "Fresno",
    "state": "CA",
    "name": "KSEQ (Q 97.1)",
    "twitter": "@Q97fm"
  },
  "Hits 96": {
    "city": "Chattanooga",
    "state": "TN",
    "name": "Hits 96",
    "twitter": "@Hits96Radio"
  },
  "4ZZZ": {
    "city": "Brisbane",
    "state": "Australia",
    "name": "4ZZZ",
    "twitter": "@4ZZZ"
  },
  "93.3 FLZ": {
    "city": "Tampa",
    "state": "FL",
    "name": "93.3 FLZ",
    "twitter": "@933FLZ"
  },
  "Now 105": {
    "city": "Norfolk",
    "state": "VA",
    "name": "Now 105",
    "twitter": "@Now1053"
  },
  "107.5 The River": {
    "city": "Nashville",
    "state": "TN",
    "name": "107.5 The River",
    "twitter": "@1075theriver"
  },
  "Hot 101.5": {
    "city": "Tampa",
    "state": "FL",
    "name": "Hot 101.5",
    "twitter": "@Hot1015"
  },
  "Power 98.3": {
    "city": "Phoenix",
    "state": "AZ",
    "name": "Power 98.3",
    "twitter": "@POWER983"
  },
  "Juize": {
    "city": "Amsterdam",
    "state": "Netherlands",
    "name": "Juize",
    "twitter": "@JuizeNL"
  },
  "Channel 933": {
    "city": "San Diego",
    "state": "CA",
    "name": "Channel 933",
    "twitter": "@Channel933"
  },
  "WPGC": {
    "city": "Washington",
    "state": "DC",
    "name": "WPGC",
    "twitter": "@WPGC"
  }
};

export default stations;