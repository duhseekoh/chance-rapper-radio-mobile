import React from 'react';
import { Font } from 'expo';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducers from './src/reducers/';
import AppWithNavigationState from './src/navigators/AppNavigator';

const middlewares = [thunk];
if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    collapsed: (getState, action, logEntry) => !logEntry.error,
  });
  middlewares.push(logger);
}

const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);

class RapperRadioApp extends React.Component {
  state = {
    fontLoaded: false,
  }
  async componentDidMount() {
    await Font.loadAsync({
      'permanent-marker': require('./assets/fonts/PermanentMarker.ttf'),
    });

    this.setState({ fontLoaded: true });
  }
  render() {
    return (
      this.state.fontLoaded ? (<Provider store={store}>
        <AppWithNavigationState />
      </Provider>) : null
    );
  }
}

AppRegistry.registerComponent('RapperRadioApp', () => RapperRadioApp);

export default RapperRadioApp;
