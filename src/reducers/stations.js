import { handleActions } from 'redux-actions';

import {
  stationsFetchSuccess,
  stationsFetchFailure,
  stationsByLatLonFetchSuccess,
  stationsByLatLonFetchFailure,
} from '../actions/stations';

import { buildCityName } from '../selectors/stations';

const initialState = {
  stationsByName: {},
  stationsByLatLon: {},
};

const stations = handleActions({
  [stationsFetchSuccess]: (state, { payload }) => {
    const stationsWithCityName = payload.map(station => ({
      ...station,
      cityName: buildCityName(station),
    }));
    return ({
      ...state,
      stationsByName: stationsWithCityName,
    });
  },

  [stationsFetchFailure]: (state, { payload }) => ({
    ...state,
    error: payload,
  }),

  [stationsByLatLonFetchSuccess]: (state, { payload }) => {
    const stationsWithCityName = payload.map(station => ({
      ...station,
      cityName: buildCityName(station),
    }));
    return ({
      ...state,
      stationsByLatLon: stationsWithCityName,
    });
  },

  [stationsByLatLonFetchFailure]: (state, { payload }) => ({
    ...state,
    error: payload,
  }),
}, initialState);

export default stations;
