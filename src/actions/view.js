import { createAction } from 'redux-actions';

export const changeText = createAction('VIEW_CHANGE_TEXT');
export const setKeyboardMargin = createAction('VIEW_SET_KEYBOARD_MARGIN');
export const toggleSuggestions = createAction('VIEW_TOGGLE_SUGGESTIONS');
export const toggleStations = createAction('VIEW_TOGGLE_STATIONS');
export const toggleInput = createAction('VIEW_TOGGLE_INPUT');
export const selectCity = createAction('VIEW_SELECT_CITY');
export const selectStation = createAction('VIEW_SELECT_STATION');
export const selectSong = createAction('VIEW_SELECT_SONG');
export const setTwitterURI = createAction('VIEW_SET_TWITTER_URI');
export const setFacebookURI = createAction('VIEW_SET_FB_URI');
export const resetView = createAction('VIEW_RESET');
export const setIsLoading = createAction('VIEW_SET_IS_LOADING');
export const resetCity = createAction('VIEW_RESET_CITY');
export const resetStation = createAction('VIEW_RESET_STATION');
