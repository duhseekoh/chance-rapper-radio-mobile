import { createAction } from 'redux-actions';
import { apiFetchSongs } from './api/songs';

export const songsFetchSuccess = createAction('SONGS_FETCH_SUCCESS');
export const songsFetchFailure = createAction('SONGS_FETCH_FAILURE');
export const setSelectedSong = createAction('SET_SELECTED_SONG');

export const fetchSongs = () => dispatch =>
  apiFetchSongs()
    .then(songs => dispatch(songsFetchSuccess(songs)))
    .catch(err => dispatch(songsFetchFailure(err)));

export const updateSongSelection = id => dispatch =>
  dispatch(setSelectedSong(id));
