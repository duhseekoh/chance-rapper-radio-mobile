import colors from '../styles/colors';

export default {
  mainScreen: {
    title: 'Rapper Radio',
    headerStyle: { backgroundColor: colors.primary.block },
    headerTitleStyle: {
      fontFamily: 'permanent-marker',
      fontSize: 20,
    },
    headerTintColor: colors.primary.text,
  },
  songScreen: {
    title: 'Rapper Radio',
    headerStyle: { backgroundColor: colors.primary.block },
    headerTitleStyle: {
      fontFamily: 'permanent-marker',
      fontSize: 20,
    },
    headerTintColor: colors.primary.text,
  },
};
