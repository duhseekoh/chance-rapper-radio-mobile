// TODO: replace with real api
// export function apiFetchSongs() {
//   const endpoint = `${AppConstants.api_root}/users`;
//   return fetch(endpoint);
// }
import { API_HOST } from '../../../env-config';

export function apiFetchSongs() { // eslint-disable-line
  return fetch(`${API_HOST}/songs`)
    .then(response =>
      (response.ok ?
        response.json() :
        Promise.reject(new Error(`${response.status} Error fetching songs`)))
    );
}
