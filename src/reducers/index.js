import nav from './nav';
import auth from './auth';
import songs from './songs';
import view from './view';
import stations from './stations';

const reducers = {
  nav,
  auth,
  songs,
  view,
  stations,
};

export default reducers;
