import { createSelector } from 'reselect';

export const selectStations = state => state.stations.stationsByName;
export const selectStationsByLatLon = state => state.stations.stationsByLatLon;
export const selectSelectedCity = state => state.view.currentSelectedCity;
export const selectSelectedStation = state => state.view.currentSelectedStation;
export const selectInputText = state => state.view.inputTextValue;

export const selectCityList = createSelector(
  selectStations,
  (stations) => {
    let cityList = [];
    Object.keys(stations).forEach((key) => {
      const station = stations[key];
      const cityName = buildCityName(station);

      if (cityList.indexOf(cityName) < 0) {
        cityList = [
          ...cityList,
          cityName,
        ];
      }
    });
    return cityList;
  }
);

export const selectSelectedStationTwitter = createSelector(
  selectSelectedStation,
  selectStations,
  (currentStation, stations) => {
    if (stations[currentStation]) {
      return stations[currentStation].twitterHandle;
    }
    return '';
  }
);

export const selectCityObjList = createSelector(
  selectStations,
  (stations) => {
    let cityObjList = [];
    let cityList = [];
    Object.keys(stations).forEach((key) => {
      const station = stations[key];
      const cityName = buildCityName(station);

      if (cityList.indexOf(cityName) < 0) {
        cityList = [
          ...cityList,
          cityName,
        ];
        cityObjList = [
          ...cityObjList,
          {
            name: cityName,
            lat: station.lat,
            lon: station.lon,
          },
        ];
      }
    });
    return cityObjList;
  }
);

export const selectLatLonByCity = createSelector(
  selectCityObjList,
  selectSelectedCity,
  (cities, selectedCity) => {
    const cityObj = cities.find(city =>
      (city.name === selectedCity)
    );
    if (cityObj) {
      return { lat: cityObj.lat, lon: cityObj.lon };
    }
    return { lat: null, lon: null };
  }
);

export const selectCitySuggestions = createSelector(
  selectInputText,
  selectCityObjList,
  (text, cities) => cities
    .filter(city => city.name.toLowerCase().startsWith(text.toLowerCase()))
    .slice(0, 5)
);

export const selectStationSuggestions = createSelector(
  selectStationsByLatLon,
  (stations) => {
    let stationList = [];
    Object.keys(stations).forEach((key) => {
      const station = stations[key];
      stationList = [
        ...stationList,
        station,
      ];
    });
    return stationList.slice(0, 5);
  }
);

export const selectStationsByCity = createSelector(
  selectStations,
  selectSelectedCity,
  (stations, city) => {
    const cityStationMap = {};

    Object.keys(stations).forEach((key) => {
      const station = stations[key];
      const cityName = buildCityName(station);

      if (cityStationMap[cityName]) {
        cityStationMap[cityName] = [
          ...cityStationMap[cityName],
          station,
        ];
      } else {
        cityStationMap[cityName] = [station];
      }
    });

    return cityStationMap[city];
  }
);

export function buildCityName(station) {
  return station.state.length >= 2 ?
    `${station.city}, ${station.state}` : `${station.city}, ${station.country}`;
}
