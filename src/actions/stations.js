import { createAction } from 'redux-actions';
import { apiFetchStations } from './api/stations';

export const stationsFetchSuccess = createAction('STATIONS_FETCH_SUCCESS');
export const stationsFetchFailure = createAction('STATIONS_FETCH_FAILURE');
export const stationsByLatLonFetchSuccess = createAction('STATIONS_FETCH_LATLON_SUCCESS');
export const stationsByLatLonFetchFailure = createAction('STATIONS_FETCH_LATLON_FAILURE');

export const fetchStations = () => dispatch =>
  apiFetchStations()
    .then(cities => dispatch(stationsFetchSuccess(cities)))
    .catch(err => dispatch(stationsFetchFailure(err)));

export const fetchStationsByLatLon = (lat, lon) => dispatch =>
  apiFetchStations(lat, lon)
    .then(cities => dispatch(stationsByLatLonFetchSuccess(cities)))
    .catch(err => dispatch(stationsByLatLonFetchFailure(err)));
